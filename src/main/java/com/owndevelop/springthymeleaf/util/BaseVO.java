/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owndevelop.springthymeleaf.util;

import java.io.Serializable;

/**
 *
 * @author iqbal
 * @param <T> Model
 * @param <V> DTO
 */
public abstract class BaseVO<T extends Serializable, V extends Serializable> {

    public abstract V convertModelToDto(T model);

    public abstract T convertDtoToModel();
}

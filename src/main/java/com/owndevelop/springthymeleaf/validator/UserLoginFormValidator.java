/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owndevelop.springthymeleaf.validator;

import com.owndevelop.springthymeleaf.dto.UserloginDTO;
import com.owndevelop.springthymeleaf.service.UserloginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author iqbal
 */
@Component
public class UserLoginFormValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(UserLoginFormValidator.class);
    private final UserloginService userLoginService;

    @Autowired
    public UserLoginFormValidator(UserloginService userLoginService) {
        this.userLoginService = userLoginService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(UserloginDTO.class);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        logger.info("validating {}", obj);
        UserloginDTO userloginDTO = (UserloginDTO) obj;
        validateUsername(userloginDTO, errors);
    }

    private void validateUsername(UserloginDTO userloginDTO, Errors errors) {
        if (userloginDTO.getUserid() == null) {
            if (userLoginService.isUserloginFoundbyusername(userloginDTO.getUsername())) {
                errors.reject("error", "User with this username already exists");
            }
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owndevelop.springthymeleaf.controller;

import com.owndevelop.springthymeleaf.dto.UserloginDTO;
import com.owndevelop.springthymeleaf.service.UserloginService;
import com.owndevelop.springthymeleaf.util.JsonResponse;
import com.owndevelop.springthymeleaf.validator.UserLoginFormValidator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author iqbal
 */
@Controller
@RequestMapping("/usermanagement/user")
public class UserController {

    private UserloginService userloginService;
    private UserLoginFormValidator userLoginFormValidator;
    
    @Autowired
    public UserController(UserloginService userloginService, UserLoginFormValidator userLoginFormValidator) {
        this.userloginService = userloginService;
        this.userLoginFormValidator = userLoginFormValidator;
    }

    @InitBinder("userLoginDTO")
    public void initUserLoginBinder(WebDataBinder binder) {
        binder.addValidators(userLoginFormValidator);
    }

    @RequestMapping
    public String getAllUser(Model model) {
        List<UserloginDTO> userloginDTOs = userloginService.getAllUserlogin();
        model.addAttribute("userLogins", userloginDTOs);
        model.addAttribute("title", "User List");
        return "/page/usermanagement/user";
    }

    @RequestMapping("/add")
    public String addUser(Model model) {
        UserloginDTO userloginDTO = new UserloginDTO();
        model.addAttribute("userLoginDTO", userloginDTO);
        model.addAttribute("title", "Add User");
        return "/page/usermanagement/userform";
    }

    @RequestMapping("/edit")
    public String editUser(Long id, Model model) {
        UserloginDTO userloginDTO = userloginService.getUserLogin(id);
        model.addAttribute("userLoginDTO", userloginDTO);
        model.addAttribute("title", "Edit User");
        return "/page/usermanagement/userform";
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public @ResponseBody
    JsonResponse actionUser(@Valid @ModelAttribute("userLoginDTO") UserloginDTO userloginDTO, BindingResult bindingResult, UriComponentsBuilder b) {
        JsonResponse res = new JsonResponse();
        if (bindingResult.hasErrors()) {
            res.setStatus("FAILED");
            res.setResult(bindingResult.getAllErrors());
            return res;
        }
        if (userloginDTO.getUserid() == null) {
            userloginDTO.setLogindate(new Date());
            userloginDTO.setCreationdate(new Date());
            userloginDTO = userloginService.insertUserlogin(userloginDTO);
        } else {
            userloginDTO.setEditdate(new Date());
            userloginDTO = userloginService.updateUserlogin(userloginDTO);
        }

        UriComponents uriComponents = b.path("/usermanagement/user/edit?id=" + userloginDTO.getUserid()).build();
        res.setStatus("SUCCESS");
        res.setResult(uriComponents.toUriString());

        return res;
    }

//    @RequestMapping(value = "/form", method = RequestMethod.POST)
//    public String actionUser(@Valid @ModelAttribute("userLoginDTO") UserloginDTO userloginDTO, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
//        if (bindingResult.hasErrors()) {
//            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.strategy", bindingResult);
//            redirectAttributes.addFlashAttribute("userLoginDTO", userloginDTO);
//            return "/page/usermanagement/userform";
//        }
//        if (userloginDTO.getUserid() == null) {
//            userloginDTO.setLogindate(new Date());
//            userloginDTO.setCreationdate(new Date());
//            userloginDTO = userloginService.insertUserlogin(userloginDTO);
//        } else {
//            userloginDTO.setEditdate(new Date());
//            userloginDTO = userloginService.updateUserlogin(userloginDTO);
//        }
//
//        return "redirect:/usermanagement/user/edit?id=" + userloginDTO.getUserid();
//    }
}

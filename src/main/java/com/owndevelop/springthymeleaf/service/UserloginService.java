/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.owndevelop.springthymeleaf.service;

import com.owndevelop.springthymeleaf.dto.UserloginDTO;
import java.util.List;

/**
 *
 * @author iqbal
 */
public interface UserloginService {
    public UserloginDTO insertUserlogin(UserloginDTO dto);
    public UserloginDTO updateUserlogin(UserloginDTO dto);
    public void deleteUserlogin(UserloginDTO dTO);
    public List<UserloginDTO> getAllUserlogin();
    public UserloginDTO getUserLogin(Long id);
    public Boolean isUserloginFoundbyusername(String username);
}

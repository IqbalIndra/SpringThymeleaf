/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.owndevelop.springthymeleaf.dao;

import com.owndevelop.springthymeleaf.model.Userlogin;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author iqbal
 */
public interface UserloginRepository extends JpaRepository<Userlogin, Long>{
    public List<Userlogin> findByusername(String username);
}

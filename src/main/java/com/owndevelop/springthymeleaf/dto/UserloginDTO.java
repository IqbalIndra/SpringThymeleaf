/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owndevelop.springthymeleaf.dto;

import com.owndevelop.springthymeleaf.model.Userlogin;
import com.owndevelop.springthymeleaf.util.BaseVO;
import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author iqbal
 */
public class UserloginDTO extends BaseVO<Userlogin, UserloginDTO> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long userid;

    @NotNull(message = "Username Must Not be Null")
    @NotEmpty(message = "Username Required")
    @Size(max = 50, message = "Username Limit < 20")
    private String username;
    
    @NotNull(message = "Firstname Must Not be Null")
    @NotEmpty(message = "Firstname Required")
    @Size(max = 50, message = "Firstname Limit < 20")
    private String firstname;
    
    @NotNull(message = "Lastname Must Not be Null")
    @NotEmpty(message = "Lastname Required")
    @Size(max = 50, message = "Lastname Limit < 20")
    private String lastname;

    private Date creationdate;

    private Date editdate;

    private Date logindate;

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Date creationdate) {
        this.creationdate = creationdate;
    }

    public Date getEditdate() {
        return editdate;
    }

    public void setEditdate(Date editdate) {
        this.editdate = editdate;
    }

    public Date getLogindate() {
        return logindate;
    }

    public void setLogindate(Date logindate) {
        this.logindate = logindate;
    }

    @Override
    public UserloginDTO convertModelToDto(Userlogin model) {
        UserloginDTO userloginDTO = new UserloginDTO();
        BeanUtils.copyProperties(model, userloginDTO);
        return userloginDTO;
    }

    @Override
    public Userlogin convertDtoToModel() {
        Userlogin userlogin = new Userlogin();
        BeanUtils.copyProperties(this, userlogin);
        return userlogin;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owndevelop.springthymeleaf.serviceImpl;

import com.owndevelop.springthymeleaf.dao.UserloginRepository;
import com.owndevelop.springthymeleaf.dto.UserloginDTO;
import com.owndevelop.springthymeleaf.model.Userlogin;
import com.owndevelop.springthymeleaf.service.UserloginService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author iqbal
 */
@Service
@Transactional
public class UserLoginServiceImpl implements UserloginService {

    @Autowired
    private UserloginRepository userloginRepository;

    @Override
    public UserloginDTO insertUserlogin(UserloginDTO dto) {
        Userlogin userLogin = dto.convertDtoToModel();
        userLogin = userloginRepository.save(userLogin);
        return dto.convertModelToDto(userLogin);
    }

    @Override
    public UserloginDTO updateUserlogin(UserloginDTO dto) {
        Userlogin userLogin = dto.convertDtoToModel();
        userLogin = userloginRepository.saveAndFlush(userLogin);
        return dto.convertModelToDto(userLogin);
    }

    @Override
    public void deleteUserlogin(UserloginDTO dto) {
        boolean isExist = userloginRepository.exists(dto.getUserid());
        if (isExist) {
            Userlogin userlogin = dto.convertDtoToModel();
            userloginRepository.delete(userlogin);
        }
    }

    @Override
    public List<UserloginDTO> getAllUserlogin() {
        List<UserloginDTO> userloginDTOs = new ArrayList<>();
        List<Userlogin> userlogins = userloginRepository.findAll();
        if(!userlogins.isEmpty()){
            for(Userlogin userlogin : userlogins){
                userloginDTOs.add(new UserloginDTO().convertModelToDto(userlogin));
            }
        }
        return userloginDTOs;
    }

    @Override
    public UserloginDTO getUserLogin(Long id) {
        Userlogin userlogin = userloginRepository.getOne(id);
        return new UserloginDTO().convertModelToDto(userlogin);
    }

    @Override
    public Boolean isUserloginFoundbyusername(String username) {
        List<Userlogin> userlogins = userloginRepository.findByusername(username);
        return (userlogins.size() > 0) ? true : false;
    }

}
